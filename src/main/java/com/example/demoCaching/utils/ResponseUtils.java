package com.example.demoCaching.utils;

import com.example.demoCaching.dto.ElementoOut;
import com.example.demoCaching.dto.ElementoOutDto;
import com.example.demoCaching.dto.HttpResponse;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

public class ResponseUtils {

    public static ResponseEntity<HttpResponse> responseAccepted(ElementoOut elemento){
        ElementoOutDto elementoOutDto = ElementoOutDto.builder().build();
        elementoOutDto.setPayload(elemento);
        elementoOutDto.setStatus(HttpStatus.OK);
        return new ResponseEntity<HttpResponse>(elementoOutDto, HttpStatus.OK);
    }

    public static ResponseEntity<HttpResponse> responseGenericError(Exception e){
        HttpResponse httpResponse = new HttpResponse();
        httpResponse.setMessaggio(e.getMessage());
        httpResponse.setStatus(HttpStatus.INTERNAL_SERVER_ERROR);
        return new ResponseEntity<HttpResponse>(httpResponse, HttpStatus.INTERNAL_SERVER_ERROR);
    }
}
