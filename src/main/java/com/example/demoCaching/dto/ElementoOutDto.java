package com.example.demoCaching.dto;

import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class ElementoOutDto extends HttpResponse{
    ElementoOut payload;
    Object errors;
}
