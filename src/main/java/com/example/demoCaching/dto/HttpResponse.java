package com.example.demoCaching.dto;

import lombok.Data;
import org.springframework.http.HttpStatus;

@Data
public class HttpResponse {
    private HttpStatus status;
    private String messaggio;
}
