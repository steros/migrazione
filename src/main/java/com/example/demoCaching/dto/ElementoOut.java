package com.example.demoCaching.dto;

import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class ElementoOut {
    private String chiave;
    private String valore;
}
