package com.example.demoCaching.controller;

import com.example.demoCaching.dto.ElementoInDto;
import com.example.demoCaching.dto.ElementoOut;
import com.example.demoCaching.dto.HttpResponse;
import com.example.demoCaching.service.CachingService;
import com.example.demoCaching.utils.ResponseUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/caching")
public class CachingController {

    @Autowired
    private CachingService cachingService;

    @PostMapping(value="/salva")
    public ResponseEntity<HttpResponse> writeData(@RequestBody ElementoInDto elementoInDto){
        try{
            ElementoOut elementoOut = cachingService.salva(elementoInDto);
            return ResponseUtils.responseAccepted(elementoOut);
        }
        catch(Exception e){
            return ResponseUtils.responseGenericError(e);
        }
    }

    @GetMapping(value="/recupera/{id}")
    public ResponseEntity<HttpResponse> readData(@PathVariable("id") String id){
        try{
            ElementoOut elementoOut = cachingService.recupera(id);
            return ResponseUtils.responseAccepted(elementoOut);
        }
        catch(Exception e){
            return ResponseUtils.responseGenericError(e);
        }
    }

    @GetMapping(value="/recupera/valore/{valore}")
    public ResponseEntity<HttpResponse> readDataByValore(@PathVariable("valore") String valore){
        try{
            ElementoOut elementoOut = cachingService.recuperaPerValore(valore);
            return ResponseUtils.responseAccepted(elementoOut);
        }
        catch(Exception e ){
            return ResponseUtils.responseGenericError(e);
        }
    }
}