package com.example.demoCaching.service;

import com.example.demoCaching.dao.ElementiRepository;
import com.example.demoCaching.dto.ElementoInDto;
import com.example.demoCaching.dto.ElementoOut;
import com.example.demoCaching.model.Elemento;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@CacheConfig(cacheNames={"elements"})
public class CachingService {

    @Autowired
    private ElementiRepository elementiRepository;

    @Caching
    public ElementoOut salva(ElementoInDto elementoInDto) {
        Elemento elemento = Elemento.builder().valore(elementoInDto.getValore()).chiave(elementoInDto.getChiave()).build();
        Elemento risposta = elementiRepository.save(elemento);
        return ElementoOut.builder().chiave(risposta.getChiave()).valore(risposta.getValore()).build();
    }

    @Cacheable(value="elements", key="#id", sync=true)
    public ElementoOut recupera(String id) {
        Optional<Elemento> risposta = elementiRepository.findById(Integer.valueOf(id));
        if(risposta.isPresent()){
            return ElementoOut.builder().chiave(risposta.get().getChiave()).valore(risposta.get().getValore()).build();
        }
        else {
            return ElementoOut.builder().build();
        }
    }

    @Cacheable(value="elements", key="#valore", sync=true)
    public ElementoOut recuperaPerValore(String valore) {
        List<Elemento> risposta = elementiRepository.selByValore(valore);
        if(!risposta.isEmpty()){
            return ElementoOut.builder().chiave(risposta.get(0).getChiave()).valore(risposta.get(0).getValore()).build();
        }
        else {
            return ElementoOut.builder().build();
        }
    }
}
